# matrix-static example-server

This project should show a minimal example of how to create a static
matrix server, using the
[matrix-static/matrix-static.gitlab.io](https://gitlab.com/matrix-static/matrix-static.gitlab.io)
javascript library.

Explore the folders and files of this repository, and read the
documentation on the library for installation and usages.

This project is currently deployed on:
- cloudflare pages [static-server.pages.dev](https://static-server.pages.dev)
- netlify pages [static-server.netlify.app](https://static-server.netlify.app)
- gitlab pages https://matrix-static.gitlab.io/static-server (not
  working, because hosted in a subfolder; see `matrix-static` repo for
  gitlab root organization repository deployment example)
- netlify pages (tbd)
- glitch.com pages (tbd)
- vercel pages (tbd)
- codesandbox.io
- any "pages provider" offering a "clean subdomain URL", and `_redirects`

## Usage
The general idea to start experimenting is to:

- clone/fork this project
- deploy to a "pages provider" (here cloudflare)
- updated the `matrix-static.json` file
- customize the `./content/` folder, to represent the server's spaces,
  rooms and events
- commit and push the changes to build and publish a new state/version
  of the server's content as matrix.org static JSON API

## Deployment
This project is deployed on
[pages.cloudflare.com](https://pages.cloudflare.com/) as a staticly
generate HTML/JSON web-site/app. It could also be netlify, or gitlab
pages (but only on the root domain of a user/organization, since other
wise the homeserver URL would have `my-user.gitlab.io/my-homserver`,
and this cannot work currently).

> The npm package `matrix-server` project's readme contains more deployment
> information

## Overwrite the `matrix-static.json` config

For example with the command:
```bash
echo '{"m.server": "static-server.netlify.app:443"}' > matrix-static.json
```

Then we can run the `build` script, with the new config (here a netlify deploy)

So for example in netlify UI we could have the following deploy command:
```bash
echo '{"m.server": "static-server.netlify.app:443"}' > matrix-static.json && npm run build
```

## Test URLs
These URL should work (more to come). Note that cloudlare `_redirects`
handling, does not preserve serving the original file (so the URL with
`.json` ending does not resolve, when it also does on gitlab pages).

- https://static-server.pages.dev/.well-known/matrix/server
- https://static-server.pages.dev/_matrix/federation/v1/version
- https://static-server.pages.dev/_matrix/federation/v1/publicRooms
- https://static-server.pages.dev/_matrix/client/v3/publicRooms

It should allow to add `static-server.pages.dev` as a new "directory
server" in the Element interface (search > explore public room > "new
server").
